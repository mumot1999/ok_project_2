import glob
import subprocess
from timeit import default_timer as timer

files = [f for f in glob.glob("./**", recursive=True)]
files = list(filter(lambda x: '.txt' in x.lower() and 'output' not in x, files))

black_list = [
    # './m2kvrptw-0.txt'
]
files = [file for file in files if file not in black_list]

passed = []
try:
    for file in files:
        print(file+"...")
        start = timer()
        subprocess.getoutput('../a.out {}'.format(file))
        print(str(timer()-start)+"s")
        response = subprocess.getoutput('wine ./ckrptw.exe {} {}'.format(file, './output.txt'))
        print(file + "->" + response)
        passed.append('ok' in response.lower())
        print()
except KeyboardInterrupt:
    pass

print("Status: " + str(all(passed)))
