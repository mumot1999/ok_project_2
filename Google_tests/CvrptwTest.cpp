#include <Node/Nodes.h>
#include <chrono>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Cvrptw.h"
using namespace testing;

TEST(ATimeTest, rc21010){
    Cvrptw instance = Cvrptw("data/rc21010.txt");

    for(int i = 1; i <= 1001; i+=1){
        Nodes nodes = instance.nodes.limit(i)->nodes;

        Cvrptw cvrptw = Cvrptw(nodes, instance.trucks);
        auto start = std::chrono::high_resolution_clock::now();
        Routes *pSolution = cvrptw.run();
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);

        ASSERT_TRUE(pSolution->is_solution_good(i-1));
        const Score &score = pSolution->get_score();
        std::cout << i << " " << duration.count() << " " << score.time << " " << score.distance << " " << pSolution->routes->size() << std::endl;

        for(auto* node : *nodes.nodes){
            node->visited = false;
        }

        delete(nodes.nodes);
        delete(pSolution);
    }
}

TEST(DrozdowskiTests, HandleFail) {
    //Truck will back to base at 120
    Nodes nodes = Nodes(2);
    nodes[0]->time_end = 100;

    nodes[1]->y = 10;
    nodes[1]->cargo = 10;
    nodes[1]->time_start = 100;
    nodes[1]->unload_duration = 10;

    Trucks trucks = Trucks(10, 1000);

    Cvrptw cvrptw = Cvrptw(nodes, trucks);

    ASSERT_EQ(0, cvrptw.is_possible());
}

TEST(DrozdowskiTests, Fail1) {
    //http://www.cs.put.poznan.pl/mdrozdowski/dyd/ok/cvrptw1.txt
    //Truck will back to base at 120
    Nodes nodes = Nodes();
    nodes.push_node(new Node(0, 0, 0, 0, 0, 100, 0));
    nodes.push_node(new Node(1,0,10,10,200,300,10));
    nodes.push_node(new Node(2,0,20,10,10,120,10));
    nodes.push_node(new Node(3,0,30,10,20,180,10));

    Trucks trucks = Trucks(10, 1000);

    Cvrptw cvrptw = Cvrptw(nodes, trucks);

    ASSERT_EQ(false, cvrptw.is_possible());
}

TEST(DrozdowskiTests, Possible2) {
    //http://www.cs.put.poznan.pl/mdrozdowski/dyd/ok/cvrptw3.txt
    //Truck will back to base at 120
    Nodes nodes = Nodes();
    nodes.push_node(new Node(0,0,0,0,0,100,0));
    nodes.push_node(new Node(1,0,10,10,10,300,10));
    nodes.push_node(new Node(2,0,20,10,20,120,10));
    nodes.push_node(new Node(3,0,30,10,30,180,10));

    Trucks trucks = Trucks(10, 1000);

    Cvrptw cvrptw = Cvrptw(nodes, trucks);

    ASSERT_EQ(true, cvrptw.is_possible());
    Routes *solution = cvrptw.run();
    ASSERT_EQ(90, solution->get_score().time);
//    ASSERT_THAT(solution->routes->at(1)->get_route(), ElementsAre(1,2,3));
}

TEST(DrozdowskiTests, Fail3) {
    //http://www.cs.put.poznan.pl/mdrozdowski/dyd/ok/cvrptw3.txt
    //Truck will back to base at 120
    Nodes nodes = Nodes();
    nodes.push_node(new Node(0,0,0,0,0,40,0));
    nodes.push_node(new Node(1,0,10,10,30,40,10));
    nodes.push_node(new Node(2,0,20,10,10,20,10));
    nodes.push_node(new Node(3,0,30,10,20,30,10));

    Trucks trucks = Trucks(10, 1000);

    Cvrptw cvrptw = Cvrptw(nodes, trucks);

    ASSERT_EQ(0, cvrptw.is_possible());
}


TEST(DrozdowskiTests, Possible4) {
    //http://www.cs.put.poznan.pl/mdrozdowski/dyd/ok/cvrptw3.txt
    Nodes nodes = Nodes();
    nodes.push_node(new Node(0,0,0,0,0,100,0));
    nodes.push_node(new Node(1,0,10,10,20,100,10));
    nodes.push_node(new Node(2,0,20,10,10,100,10));
    nodes.push_node(new Node(3,0,30,10,20,100,10));

    Trucks trucks = Trucks(10, 10);

    Cvrptw cvrptw = Cvrptw(nodes, trucks);

    ASSERT_EQ(1, cvrptw.is_possible());
    Routes *pSolution = cvrptw.run();
    double time = pSolution->get_score().time;
    ASSERT_EQ(160, time);
}

TEST(DrozdowskiTests, HandleOk) {
    //Truck will back to base at 120
    Nodes nodes = Nodes(2);
    nodes[0]->time_end = 120;
    nodes[0]->x = 0;
    nodes[0]->y = 0;

    nodes[1]->x = 0;
    nodes[1]->y = 10;
    nodes[1]->cargo = 10;
    nodes[1]->time_start = 100;
    nodes[1]->time_end = 150;
    nodes[1]->unload_duration = 10;

    Trucks trucks = Trucks(1, 1000);

    Cvrptw cvrptw = Cvrptw(nodes, trucks);
    ASSERT_EQ(10, cvrptw.nodes[1]->y);
    double time = cvrptw.run()->get_score().time;
    ASSERT_EQ(120, time);
}

TEST(DrozdowskiTests, test_read_file) {
    Cvrptw cvrptw = Cvrptw("data/cvrptw2.txt");
    ASSERT_EQ(4, cvrptw.nodes.nodes->size());
    ASSERT_EQ(10, cvrptw.trucks.trucks.size());
    ASSERT_EQ(90, cvrptw.run()->get_score().time);
}

//TEST(DrozdowskiTests, test_m2kvrptwtxt) {
//    Cvrptw cvrptw = Cvrptw("data/m2kvrptw-0.txt");
//    ASSERT_EQ(2001, cvrptw.nodes.nodes->size());
////    ASSERT_EQ(500, cvrptw.trucks.trucks.size());
////    ASSERT_EQ(500, cvrptw.trucks.trucks[0].capacity);
//    ASSERT_EQ(2891, cvrptw.nodes[1997]->time_end);
//    ASSERT_EQ(4520131.2, cvrptw.run());
//}

TEST(MyTests, test_1) {
    Nodes nodes = Nodes();
    nodes.push_node(new Node(0,0,0,0,0,0,0));
    nodes.push_node(new Node(1,20,0,80,100,150,10));
    nodes.push_node(new Node(2,30,0,20,40,40,10));

    Trucks trucks = Trucks(1, 100);

    Cvrptw cvrptw = Cvrptw(nodes, trucks);

    Routes *solution = cvrptw.run();
    const Score &score = solution->get_score();
    ASSERT_EQ(130, score.time);
    ASSERT_EQ(60, score.distance);
//    ASSERT_THAT(solution->routes->at(0)->get_route(), ElementsAre(2,1));
}

TEST(SintefTests, 1_2_1){
    Cvrptw cvrptw = Cvrptw("data/C1_2_1.TXT");
    Routes *pSolution = cvrptw.run();
    ASSERT_TRUE(pSolution->is_solution_good(cvrptw.nodes.size()-1));
    const Score &score = pSolution->get_score();
    double time = score.time;
    double distance = score.distance;
    ASSERT_EQ(2704.57, distance);
}

TEST(DrozdowskiTests, m2kvrptw){
    Cvrptw cvrptw = Cvrptw("data/m2kvrptw-0.txt");
    Routes *pSolution = cvrptw.run();
    ASSERT_TRUE(pSolution->is_solution_good(cvrptw.nodes.size()-1));
    double time = pSolution->get_score().time;
    ASSERT_GE(1048, pSolution->routes->size());
    ASSERT_GE(4520131.3, time);
}

