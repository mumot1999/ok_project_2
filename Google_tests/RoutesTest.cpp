#include <Node/Nodes.h>
#include <Route/Route.h>
#include <Route/Routes.h>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace testing;

class RoutesTest : public ::testing::Test
{
protected:
    Nodes* nodes{};

    virtual void SetUp()
    {
//        3------2
//
//
//        0------1-----4
        nodes = new Nodes();

        nodes->push_back(new Node(0, 0, 0, 0, 0, 100, 0));
        nodes->push_back(new Node(1, 10, 0, 1, 0, 100, 1));
        nodes->push_back(new Node(2, 10, 10, 1, 0, 100, 1));
        nodes->push_back(new Node(3, 0, 10, 1, 0, 100, 1));
        nodes->push_back(new Node(4, 20, 0, 1, 0, 100, 1));
    }

    virtual void TearDown()
    {
        delete nodes;
    }
};

TEST_F(RoutesTest, it_should_put_node) {

    auto *routes = new Routes(nodes->at(0), 2);

    for(auto* node : *nodes->no_depot()){
        routes->put_node(node);
    }

    ASSERT_EQ(routes->size(), 2);
    ASSERT_THAT(routes->at(0)->ids(), ElementsAre(1,2));
    ASSERT_THAT(routes->at(1)->ids(), ElementsAre(3,4));
    ASSERT_LE(routes->get_score().time, 90.5029);
}

