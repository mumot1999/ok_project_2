#include <Node/Nodes.h>
#include <Route/Route.h>
#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace testing;

class RouteTest : public ::testing::Test
{
protected:
    Nodes* nodes{};

    virtual void SetUp()
    {
//        3------2
//
//
//        0------1
        nodes = new Nodes();

        nodes->push_back(new Node(0, 0, 0, 0, 0, 100, 0));
        nodes->push_back(new Node(1, 10, 0, 1, 0, 100, 1));
        nodes->push_back(new Node(2, 10, 10, 1, 0, 100, 1));
        nodes->push_back(new Node(3, 0, 10, 1, 0, 100, 1));
    }

    virtual void TearDown()
    {
        delete nodes;
    }
};

TEST_F(RouteTest, it_should_put_node_on_right_place){

    auto* route = new Route(nodes->at(0), 2);

    route->put_node(nodes->at(1));
    try{
        route->put_node(nodes->at(1)); //cant add this node to route because it is already in route
    }catch (...){}

    route->put_node(nodes->at(2));

    try{
        route->put_node(nodes->at(3)); //cant add this node to route because of max_cargo
    }catch (...){}

    ASSERT_THAT(route->ids(), ElementsAre(1,2));
}

TEST_F(RouteTest, it_should_put_node_at_best_place){

    auto* route = new Route(nodes->at(0), 3);

    route->put_node(nodes->at(1));
    route->put_node(nodes->at(3));
    route->put_node(nodes->at(2));

    ASSERT_THAT(route->ids(), ElementsAre(1,2,3));
}

TEST_F(RouteTest, it_should_put_node_at_best_place2){

    auto* route = new Route(nodes->at(0), 3);

    route->put_node(nodes->at(2));
    route->put_node(nodes->at(3));
    route->put_node(nodes->at(1));

    ASSERT_THAT(route->ids(), ElementsAre(1,2,3));
}


