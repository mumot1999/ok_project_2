#include <Cvrptw.h>
#include "gtest/gtest.h"
#include "Node/Nodes.h"

TEST(NodesTest, HandleInit) {
    Nodes nodes = Nodes(5);
    nodes.get_node(1)->x = 20;
    ASSERT_EQ(5, nodes.size());
    ASSERT_EQ(20, nodes[1]->x);
}

TEST(NodesTest, test_sort_by_distance) {
    Nodes nodes = Nodes(4);
    nodes[0]->x = 10;
    nodes[0]->y = 10;
    nodes[1]->x = 20;
    nodes[1]->y = 20;
    nodes[2]->x = 30;
    nodes[2]->y = 30;
    nodes[3]->x = 12;
    nodes[3]->y = 12;

    ASSERT_FALSE(nodes[0]->visited);
    nodes[0]->visited = true;
    ASSERT_TRUE(nodes[0]->visited);
    nodes[0]->visited = false;

    Nodes* closest_nodes = nodes.sort_by_distance(nodes.get_node(0));
    ASSERT_EQ(3, closest_nodes->get_node(0)->id);
    ASSERT_EQ(1, closest_nodes->get_node(1)->id);
    ASSERT_EQ(2, closest_nodes->get_node(2)->id);
    closest_nodes->get_node(1)->visited = true;
    ASSERT_TRUE(nodes[1]->visited);
}


TEST(NodesTest, test_sort_by_time_end) {
    Nodes nodes = Nodes(4);

    nodes[1]->time_end = 20;
    nodes[2]->time_end = 30;
    nodes[3]->time_end = 12;

    Nodes* sorted = nodes.sort_by_time_end();
    ASSERT_EQ(3, sorted->get_node(0)->id);
    ASSERT_EQ(1, sorted->get_node(1)->id);
    ASSERT_EQ(2, sorted->get_node(2)->id);
}

TEST(NodesTest, test_get_unvisited_nodes) {
    Nodes nodes = Nodes(3);
    nodes[0]->visited = true;

    Nodes *unvisited_nodes = nodes.get_unvisited_nodes();
    ASSERT_EQ(2, unvisited_nodes->size());
    ASSERT_EQ(1, (*unvisited_nodes)[0]->id);
    ASSERT_EQ(2, (*unvisited_nodes)[1]->id);

    //change also in original object
    unvisited_nodes->operator[](0)->x = 200;
    ASSERT_EQ(200, nodes[1]->x);
}

TEST(NodesTest, test_calclulate_in_out) {
    Nodes nodes = Nodes();

    nodes.push_node(new Node(0,0,0,0,0,130,0));
    nodes.push_node(new Node(1,10,0,10,50,70,20));
    nodes.push_node(new Node(2,20,0,10,20,30,10));
    nodes.push_node(new Node(3,10,18,20,70,100,20));

    nodes.calculate_in_out(5);

    ASSERT_EQ(1, nodes[1]->in->size());
    ASSERT_EQ(1, nodes[1]->in->size());
    ASSERT_EQ(2, nodes[1]->in->at(0)->id);
    ASSERT_EQ(3, nodes[1]->out->at(0)->id);

    ASSERT_EQ(0, nodes[2]->in->size());
    ASSERT_EQ(2, nodes[2]->out->size());
    ASSERT_EQ(1, nodes[2]->out->at(0)->id);
    ASSERT_EQ(3, nodes[2]->out->at(1)->id);

    ASSERT_EQ(2, nodes[3]->in->size());
    ASSERT_EQ(1, nodes[3]->in->at(0)->id);
    ASSERT_EQ(2, nodes[3]->in->at(1)->id);
    ASSERT_EQ(0, nodes[3]->out->size());

    nodes[1]->in->at(0)->id = 10;
    ASSERT_EQ(10, nodes[3]->in->at(1)->id);

}