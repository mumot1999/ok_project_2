#include "gtest/gtest.h"
#include "Node/Node.h"

TEST(NodeTest, HandleInit) {
    Node node = Node(1, 10, 20, 5, 0, 10, 50);
    ASSERT_EQ(1, node.id);
}

TEST(NodeTest, test_distance_to_node) {
    Node node = Node(1, 10, 20, 5, 0, 10, 50);
    Node node1 = Node(1, 13, 24, 5, 0, 10, 50);

    ASSERT_EQ(5, node.distance_to_node(&node1));
}