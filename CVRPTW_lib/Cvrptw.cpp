//
// Created by bartek on 27.10.19.
//

#include <Node/Nodes.h>
#include <sstream>
#include <fstream>
#include <Route/Routes.h>
#include <exceptions/Exception.h>
#include "Cvrptw.h"
#include "stdexcept"

Cvrptw::Cvrptw(Nodes nodes) {
    this->nodes=nodes;
}

std::vector<std::string> explode(std::string const & s, char delim)
{
    std::vector<std::string> result;
    std::istringstream iss(s);

    for (std::string token; std::getline(iss, token, delim); )
    {
        token.erase(token.find_last_not_of("\n\r")+1);
        if(token.length())
            result.push_back(std::move(token));
    }

    return result;
}

Cvrptw::Cvrptw(std::string path) {
    std::ifstream infile(path);

    std::string line;
    int x= 0;
    nodes = Nodes();
    char arr[] = { ' ', '\t' };
    while (std::getline(infile, line))
    {

        for(char delimiter : arr){
            auto v = explode(line, delimiter);
            try{
                if(v.size() >=2){
                    std::vector <int> data;
                    for(auto& n : v)
                        data.push_back(std::stoi(n));

                    if(data.size() == 2){
                        max_cargo = data[1];
                    }else if(data.size() == 7){
                        nodes.push_back(new Node(data[0], data[1], data[2], data[3], data[4], data[5], data[6]));
                    };
                    break;
                }
            }catch(...){

            }
        }
    }
}

