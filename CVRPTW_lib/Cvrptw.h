//
// Created by bartek on 27.10.19.
//

#ifndef TESTING_TEST_CVRPTW_H
#define TESTING_TEST_CVRPTW_H

#include "Route/Routes.h"
#include "Node/Nodes.h"

class Cvrptw {
public:
    Nodes nodes;
    int max_cargo;
    Cvrptw(std::string path);
    Cvrptw(Nodes nodes);

};


#endif //TESTING_TEST_CVRPTW_H
