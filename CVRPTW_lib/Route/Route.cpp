//
// Created by bartek on 08.11.19.
//

#include <memory>
#include "Route.h"

Route::Route(Node* depot, int cargo) {
    this->current_time = 0;
    this->max_cargo = cargo;

    this->depot = depot;

}

Node *Route::actual_position() {
    if(empty())
        return this->depot;
    else
        return back();
}

void Route::put_node(Node* new_node) {

    for(auto* node : *this){
        if(node->id == new_node->id)
            return;
    }

    auto* copy = get_copy();
    Nodes* best_route = copy;
    double min_cost = 0;

    for(unsigned long i = 0; i <= size(); i++){
        insert(end()-i, new_node);

        if(check_and_update_route_info()){
            if(min_cost == 0){
                best_route = get_copy();
                min_cost = current_time;
            }else{
                if(current_time < min_cost){
                    delete best_route;
                    best_route = get_copy();
                    min_cost = current_time;
                }
            }
        }
        set_data(copy);
    }

    clear();
    resize(0);
    set_data(best_route);
    if(best_route == copy){
        delete best_route;
        throw VisitNodeException();
    }
    delete copy;
    delete best_route;
    current_time = min_cost;
}

bool Route::check_and_update_route_info() {

    Node* last_node = this->depot;
    if(last_node->id != 0){
        throw std::runtime_error("Wrong depot!");
    }

    double time = 0;
    int cargo = 0;

    for(auto* node : *this){

        cargo += node->cargo;
        if(cargo > this->max_cargo)
            return false;

        time += last_node->distance_to_node(node);
        if(time > node->time_end)
            return false;
        else if (time < node->time_start){
            time = node->time_start;
        }

        time += node->unload_duration;

        last_node = node;
    }

    time += last_node->distance_to_node(depot);

    if(time > depot->time_end)
        return false;

    this->current_time = time;

    return true;
}
