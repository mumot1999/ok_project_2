//
// Created by bartek on 17.11.19.
//

#ifndef TESTING_TEST_ROUTES_H
#define TESTING_TEST_ROUTES_H


#include <vector>
#include <Node/Nodes.h>
#include <list>
#include "Route.h"

struct Score {
    double time;
    double distance;
};

class Routes : public std::vector<Route*>{
public:
    Node* depot;
    int cargo;
    double time;

    explicit Routes(Node *depot, int cargo);

    ~Routes(){
        for(auto* route: *this) {
            delete route;
        }
        resize(0);
    }

    Routes* get_copy();
    Score get_score();

    void put_node(Node* new_node);
    Route *create_new_route();

    void write_to_file(const char file_name[11]);

    void put_node_at_route(int route_id, Node *node);

    void set_data(Routes *pRoutes);
};


#endif //TESTING_TEST_ROUTES_H
