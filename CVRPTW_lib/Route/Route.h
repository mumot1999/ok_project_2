//
// Created by bartek on 08.11.19.
//

#ifndef TESTING_TEST_ROUTE_H
#define TESTING_TEST_ROUTE_H

#include <Node/Nodes.h>
#include <exceptions/Exception.h>

class Route : public Nodes{
public:
    Route(Node* depot, int cargo);
    ~Route(){
        resize(0);
    }

    double current_time;
    int max_cargo;

    Node *depot;

    void visit_node(Node* node);
    bool check_and_update_route_info();
    Node *actual_position();

    void put_node(Node* node);
};


#endif //TESTING_TEST_ROUTE_H
