//
// Created by bartek on 17.11.19.
//

#include <exceptions/Exception.h>
#include <fstream>
#include <iomanip>
#include "Routes.h"
#include "Route.h"

Score Routes::get_score(){
    Score score = {0, 0};

    for (auto *route : *this) {
        score.time += route->current_time;
    }

    return score;
}

Routes::Routes(Node *depot, int cargo) {
    this->depot = depot;
    this->cargo = cargo;
}

Route* Routes::create_new_route(){
    auto *route = new Route(this->depot, this->cargo);
    this->push_back(route);
    return route;
}

void Routes::write_to_file(const char *file_name) {
    std::ofstream outfile;
    outfile.open(file_name, std::ios::out | std::ios::trunc );

    outfile << this->size() << " ";
    outfile << std::fixed << std::setprecision(8) << get_score().time << std::endl;

    for(auto* route : *this){
        for(auto* node : *route){
            outfile << node->id << " ";
        }
        outfile << std::endl;
    }

    outfile.close();
}

void Routes::put_node(Node *new_node) {
    auto* copy = get_copy();

    Routes* best_routes = nullptr;
    double best_time = 0;

    unsigned long size1 = size();
    for(int i = 0; i <= size1; i++){
        try{
            put_node_at_route(i, new_node);
            if(best_time == 0){
                best_routes = get_copy();
                best_time = get_score().time;
            } else{
                double _time = get_score().time;
                if(_time < best_time){
                    delete best_routes;
                    best_routes = get_copy();
                    best_time = _time;
                }
            }
        }catch(VisitNodeException &e){

        }

        set_data(copy);
    }
    if(best_routes == nullptr){
        throw VisitNodeException();
    }
    set_data(best_routes);
    time = best_time;
    delete best_routes;
    delete copy;
}

Routes *Routes::get_copy() {
    auto* copy = new Routes(depot, cargo);
    for(auto* route: *this){
        auto* copy_route = new Route(depot, cargo);
        Nodes *pNodes = route->get_copy();
        copy_route->set_data(pNodes);
        delete pNodes;
        copy_route->current_time = route->current_time;
        copy->push_back(copy_route);
    }
    return copy;
}

void Routes::put_node_at_route(int route_id, Node *node) {
    if(route_id >= size())
        create_new_route();
    this->at(route_id)->put_node(node);
}

void Routes::set_data(Routes *pRoutes) {

    for(auto* route: *this)
        delete route;

    clear();
    resize(0);
    auto* copy = pRoutes->get_copy();

    for(auto* route: *copy){
        push_back(route);
    }
}
