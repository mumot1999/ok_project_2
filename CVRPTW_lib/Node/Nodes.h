//
// Created by bartek on 27.10.19.
//

#ifndef TESTING_TEST_NODES_H
#define TESTING_TEST_NODES_H


#include <algorithm>
#include <functional>
#include <utility>
#include <vector>
#include <list>
#include "Node.h"

class Nodes : public std::vector<Node*> {
public:
    Nodes();

    Nodes *filter_nodes(std::function<bool(Node *)> pFunction);
    Nodes *sort_nodes(std::function<bool(Node *, Node *)> pFunction);

    Nodes* sort_by_distance(Node* node);
    Nodes* sort_by_time_end();

    Nodes* limit(unsigned long limit);

    Nodes * get_copy();
    void set_data(Nodes *data);
    bool has_node(Node *node);
    Nodes *no_depot();
    std::list <int> nodes_list();
    int cargo();

    std::vector<int> ids();
};


#endif //TESTING_TEST_NODES_H
