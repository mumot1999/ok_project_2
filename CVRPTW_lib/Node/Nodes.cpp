//
// Created by bartek on 27.10.19.
//


#include "Nodes.h"

Nodes::Nodes(){};

Nodes *Nodes::filter_nodes(std::function<bool(Node*)> pFunction) {
    auto* new_nodes = new Nodes();
    std::copy_if(begin(),
                 end(),
                 std::back_inserter(*(new_nodes)),
                 std::move(pFunction));
    return new_nodes;
}

Nodes *Nodes::sort_nodes(std::function<bool(Node*, Node*)> pFunction) {
    std::sort(begin(), end(), std::move(pFunction));
    return this;
}

Nodes* Nodes::sort_by_distance(Node* node) {
    return this->filter_nodes([node](Node* node1){
        return !(node1->id == node->id || node1->id == 0);
    })->sort_nodes([node](Node* a, Node* b) {return a->distance_to_node(node) < b->distance_to_node(node); });
}

Nodes * Nodes::sort_by_time_end() {
    return this->filter_nodes([](Node* node){
        return node->id != 0;
    })->sort_nodes([](Node* a, Node* b) {
        return a->time_end < b->time_end;
    });
}

Nodes *Nodes::limit(unsigned long limit) {
    auto* new_nodes = new Nodes();
    std::copy(begin(),
                 end(),
                 std::back_inserter(*(new_nodes)));
    new_nodes->resize(limit < new_nodes->size() ? limit : new_nodes->size());
    return new_nodes;
}

bool Nodes::has_node(Node *node) {
    for(auto* visited_node : *this){
        if(visited_node->id == node->id)
            return true;
    }
    return false;
}

Nodes *Nodes::no_depot() {
    return this->filter_nodes([](Node* node){
        return node->id > 0;
    });
}

int Nodes::cargo() {
    int cargo = 0;
    for(auto* node : *this){
        cargo += node->cargo;
    };
    return cargo;
}

std::list<int> Nodes::nodes_list() {
    std::list <int> ids;
    for(auto* node : *this){
        ids.push_back(node->id);
    }
    return ids;
}

Nodes * Nodes::get_copy() {
    auto* copy = new Nodes();
    for(auto* node : *this){
        copy->push_back(node);
    }
    return copy;
}

std::vector<int> Nodes::ids() {
    std::vector<int> ids;

    for(auto* node : *this){
        ids.push_back(node->id);
    }
    return ids;
}

void Nodes::set_data(Nodes *data) {
    this->clear();
    resize(0);
    for(auto* node : *data)
        this->push_back(node);
}


