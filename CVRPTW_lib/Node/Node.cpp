//
// Created by bartek on 26.10.19.
//

#include "Node.h"
#include <math.h>

Node::Node(){
}

Node::Node(int id, int x, int y, int cargo, int time_start, int time_end, int unload_duration){
    this->id = id;
    this->x = x;
    this->y = y;
    this->cargo = cargo;
    this->time_start = time_start;
    this->time_end = time_end;
    this->unload_duration = unload_duration;
}

Node::Node(int id) {
    this->id = id;
}

double Node::distance_to_node(Node* node) {
    long x_delta = abs(this->x - node->x);
    long y_delta = abs(this->y - node->y);

    return pow(pow(x_delta, 2) + pow(y_delta, 2), 0.5);
};

