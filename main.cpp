#include <iostream>
#include <Cvrptw.h>
#include <exceptions/Exception.h>
#include <fstream>
#include "CVRPTW_lib/Route/Routes.h"

int main(int argc, char* argv[]) {
    Cvrptw cvrptw = Cvrptw(argv[1]);
    std::cout << "Wczytano " << cvrptw.nodes.size() << "wierzchołków" << std::endl;

    try{
        auto* routes = new Routes(cvrptw.nodes.at(0), cvrptw.max_cargo);
        for(auto* node: *cvrptw.nodes.no_depot()){
            routes->put_node(node);
        }
        routes->write_to_file("output.txt");
    }catch(VisitNodeException &e){
        std::ofstream outfile;
        outfile.open("output.txt", std::ios::out | std::ios::trunc );
        outfile << "-1";
        outfile.close();
    }

    return 0;
}